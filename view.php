<?php
include 'partials/header.php';
require __DIR__ . '/tickets/tickets.php';

if (!isset($_GET['id'])) {
    include "partials/not_found.php";
    exit;
}
$ticketId = $_GET['id'];

$ticket = getTicketById($ticketId);
if (!$ticket) {
    include "partials/not_found.php";
    exit;
}

?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>View Ticket:</h3>
        </div>
        <div class="card-body">
            <a class="btn btn-secondary" href="update.php?id=<?php echo $ticket['id'] ?>">Update</a>
            <form style="display: inline-block" method="POST" action="delete.php">
                <input type="hidden" name="id" value="<?php echo $ticket['id'] ?>">
                <button class="btn btn-danger">Delete</button>
            </form>
        </div>
        <table class="table">
            <tbody>
            <tr>
                <th>Text:</th>
                <td><?php echo $ticket['text'] ?></td>
            </tr>
            <tr>
                <th>Date:</th>
                <td><?php echo $ticket['date'] ?></td>
            </tr>
            <tr>
                <th>Severite:</th>
                <td><?php echo $ticket['severite'] ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<?php include 'partials/footer.php' ?>
