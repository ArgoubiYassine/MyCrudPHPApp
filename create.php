<?php
include 'partials/header.php';
require __DIR__ . '/tickets/tickets.php';


$ticket = [
    'id' => '',
    'text' => '',
    'date' => '',
    'severite' => '',
    
];

$errors = [
    'text' => '',
    'date' => '',
    'severite' => '',
];
$isValid = true;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ticket = array_merge($ticket, $_POST);

    $isValid = validateTicket($ticket, $errors);

    if ($isValid) {
        $ticket = createTicket($_POST);

        header("Location: index.php");
    }
}

?>

<?php include '_form.php' ?>

