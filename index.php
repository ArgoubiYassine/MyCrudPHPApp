<?php
require 'tickets/tickets.php';

$tickets = getTickets();

include 'partials/header.php';
?>


<div class="container">
    <p>
        <a class="btn btn-success" href="create.php">Create new Ticket</a>
    </p>

    <table class="table">
        <thead>
        <tr>
            <th>Text</th>
            <th>Date</th>
            <th>Severite</th>
          
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tickets as $ticket): ?>
            <tr>
                <td><?php echo $ticket['text'] ?></td>
                <td><?php echo $ticket['date'] ?></td>
                <td><?php echo $ticket['severite'] ?></td>
                
                <td>
                    <a href="view.php?id=<?php echo $ticket['id'] ?>" class="btn btn-sm btn-outline-info">View</a>
                    <a href="update.php?id=<?php echo $ticket['id'] ?>"
                       class="btn btn-sm btn-outline-secondary">Update</a>
                    <form method="POST" action="delete.php">
                        <input type="hidden" name="id" value="<?php echo $ticket['id'] ?>">
                        <button class="btn btn-sm btn-outline-danger">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach;; ?>
        </tbody>
    </table>
</div>

<?php include 'partials/footer.php' ?>

