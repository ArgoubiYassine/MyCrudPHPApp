<?php


function getTickets()
{
    return json_decode(file_get_contents(__DIR__ . '/tickets.json'), true);
}

function getTicketById($id)
{
    $tickets = getTickets();
    foreach ($tickets as $ticket) {
        if ($ticket['id'] == $id) {
            return $ticket;
        }
    }
    return null;
}

function createTicket($data)
{
    $tickets = getTickets();

    $data['id'] = rand(1000000, 2000000);

    $tickets[] = $data;

    putJson($tickets);

    return $data;
}

function updateTicket($data, $id)
{
    $updateTicket = [];
    $tickets = getTickets();
    foreach ($tickets as $i => $ticket) {
        if ($ticket['id'] == $id) {
            $tickets[$i] = $updateTicket = array_merge($ticket, $data);
        }
    }

    putJson($tickets);

    return $updateTicket;
}

function deleteTicket($id)
{
    $tickets = getTickets();

    foreach ($tickets as $i => $ticket) {
        if ($ticket['id'] == $id) {
            array_splice($tickets, $i, 1);
        }
    }

    putJson($tickets);
}

function putJson($tickets)
{
    file_put_contents(__DIR__ . '/tickets.json', json_encode($tickets, JSON_PRETTY_PRINT));
}

function validateTicket($ticket, &$errors)
{
    $isValid = true;
    // Start of validation
    if (!$ticket['date']) {
        $isValid = false;
        $errors['date'] = 'Date is required';
    }
    if (!$ticket['text'] ) {
        $isValid = false;
        $errors['text'] = 'Text is required ';
    }
    if (!$ticket['severite']) {
        $isValid = false;
        $errors['severite'] = 'Severite is required';
    }
    // End Of validation

    return $isValid;
}
