<?php
include 'partials/header.php';
require __DIR__ . '/tickets/tickets.php';

if (!isset($_GET['id'])) {
    include "partials/not_found.php";
    exit;
}
$ticketId = $_GET['id'];

$ticket = getTicketById($ticketId);
if (!$ticket) {
    include "partials/not_found.php";
    exit;
}

$errors = [
    'text' => "",
    'date' => "",
    'severite' => "",
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ticket = array_merge($ticket, $_POST);

    $isValid = validateTicket($ticket, $errors);

    if ($isValid) {
        $ticket = updateTicket($_POST, $ticketId);
        header("Location: index.php");
    }
}

?>

<?php include '_form.php' ?>
