<?php
include 'partials/header.php';
require __DIR__ . '/tickets/tickets.php';


if (!isset($_POST['id'])) {
    include "partials/not_found.php";
    exit;
}
$ticketId = $_POST['id'];
deleteTicket($ticketId);

header("Location: index.php");
