<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>
                <?php if ($ticket['id']): ?>
                    Update Ticket 
                <?php else: ?>
                    Create new Ticket
                <?php endif ?>
            </h3>
        </div>
        <div class="card-body">

            <form method="POST" enctype="multipart/form-data"
                  action="">
                <div class="form-group">
                    <label>Text</label>
                    <input name="text" value="<?php echo $ticket['text'] ?>"
                           class="form-control <?php echo $errors['text'] ? 'is-invalid' : '' ?>">
                    <div class="invalid-feedback">
                        <?php echo  $errors['text'] ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Date</label>
                    <input name="date" type="date" value="<?php echo $ticket['date'] ?>"
                           class="form-control <?php echo $errors['date'] ? 'is-invalid' : '' ?>">
                    <div class="invalid-feedback">
                        <?php echo  $errors['date'] ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Severite</label>
                    <SELECT name="severite" size="1" class="form-control" <?php echo $errors['severite'] ? 'is-invalid' : '' ?>">
                        <OPTION>bas
                        <OPTION>normal
                        <OPTION>urgent
                    </SELECT>
                    <div class="invalid-feedback">
                        <?php echo  $errors['severite'] ?>
                    </div>
                </div>

                <button class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
